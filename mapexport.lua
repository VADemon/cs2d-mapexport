addhook("parse", "mapexport.onParse")

mapexport = {}
mapexport.mapexport = {}
mapexport.listexport = {}
mapexport.logLines = {}

function mapexport.onParse(txt)
	txt = txt:lower()

	local cmdString, input_osSearchShift, os_type
	
	local firstWord, secondWord = txt:match("(%w+) *(%w*)")
	if firstWord == "mapexport" or firstWord == "listexport" then
		cmdString = firstWord
	else
		return 0
	end
	
	---
	
	local input_os = secondWord or ""
	
	if input_os == "win" or input_os == "windows" or os.getenv("os") == "Windows_NT" then
		os_type = "windows"
		
	elseif input_os == "linux" or input_os == "unix" or os.getenv("OSTYPE") == "linux-gnu" or os.getenv("OSTYPE"):find("BSD$")
		or input_os == "mac" or input_os == "macos" or input_os == "os x" or os.getenv("OSTYPE") == "darwin" then
		os_type = "unix"
	else
		print("�255100100Operating system not specified! Defaulting to unix!")
		os_type = "unix"
	end
	
	-- define OS-specific function
	mapexport.mkdir = mapexport["mkdir_" .. os_type]
	mapexport.rmdir = mapexport["rmdir_" .. os_type]
	mapexport.copy  = mapexport["copy_"  .. os_type]
	
	if cmdString == "mapexport" then
		print("�100255100Exporting map resources under " .. os_type)
		mapexport.doMapexport( os_type, map("name") )
	else	-- listexport
		print("�100255100Exporting list of resources under " .. os_type)
		mapexport.doListexport( os_type, map("name") )
	end
	
	--print("Returning 2!")
	return 2
end

function mapexport.onLog(text)
	table.insert(mapexport.logLines, text)
end

function mapexport.captureConsoleOutput(os_type, targetFolder)
	addhook("log", "mapexport.onLog")
	parse("resources")
	freehook("log", "mapexport.onLog")
	
	local logLines = mapexport.logLines
	mapexport.logLines = {}
	local folderList, fileList, resTotal = {}, {}, nil
	local separator = os_type == "windows" and "\\" or "/"	-- use \ if it's windows, else /
	
	--	MANUALLY ADD THE .map and MAP's .lua
	folderList[ "maps" .. separator ] = false	-- create "maps/" folder
	folderList[ "gfx" .. separator .."tiles" .. separator ] = false	-- gfx/tiles/
	fileList[1] = 'maps' .. separator .. map("name") .. '.map'
	fileList[2] = 'maps' .. separator .. map("name") .. '.lua'
	fileList[3] = 'maps' .. separator .. map("name") .. '.txt'
	fileList[4] = 'gfx' .. separator .. 'tiles' .. separator .. map("tileset")
	
	for	i = 1, #logLines do
		local line = logLines[i]
		
		-- capture expected amount of resources
		if not resTotal then
			local num = line:match("^ext%. res%. total: (%d+)")
			if num then
				resTotal = tonumber(num)
			end
		end
		
		local dir = line:match("^* .-%): (.+[\\/])")
		if dir then
			if os_type == "windows" then
				dir = dir:gsub("/", "\\")
			else -- cs2d maps saved mixed-style path separators, both / and \ possible in one map
				dir = dir:gsub("\\", "/")
			end
			-- use [key]=value structure because we don't need multiple folder entries
			folderList[ dir ] = false
		end
		
		local file = line:match("^* .-%): (.-) %(")
		if file then
			if os_type == "windows" then
				file = file:gsub("/", "\\")
			else
				file = file:gsub("\\", "/")
			end
			fileList[ #fileList + 1 ] = file
		end
	end
	
	if not resTotal then
		resTotal = -1
		print("�255016016Could not find \"res. total:\"! Export may not be reliable if log output changed! Contact the author for a fix")
	end
		
	--[[print("�200200000Printing captured data")
	for k,v in pairs(folderList) do
		print("folder", k, v)
	end
	
	for k,v in pairs(fileList) do
		print("file", k, v)
	end]]
	
	return folderList, fileList, resTotal
end

function mapexport.mkdir_windows(dir, k)
	os.execute('mkdir "' .. dir .. "\\" .. k .. '"')
end

function mapexport.mkdir_unix(dir, k)
	os.execute('mkdir -p \'' .. targetFolder .. "/" .. k .. '\'')
end

function mapexport.rmdir_windows(dir)
	os.execute('rmdir /S /Q "' .. dir .. '"')
end

function mapexport.rmdir_unix(dir)
	os.execute('rm -rf \'' .. dir .. '\'')
end

function mapexport.copy_windows(targetFolder, v)
	os.execute('copy "'.. v ..'" "'.. targetFolder .. "\\" .. v .. '"')
end

function mapexport.copy_unix(targetFolder, v)
	os.execute('cp \''.. v ..'\' \''.. targetFolder .. "/" .. v .. '\'')
end

-- MAPEXPORT

function mapexport.doMapexport(os_type, targetFolder)

	local _EXECUTE = os.execute
	
	os.execute = function (arg)
		print("�100100255   " .. arg)
		_EXECUTE(arg)
	end
	----
	
	local folderCount = 0	-- count folders for later output
	local folderList, fileList, resTotal = mapexport.captureConsoleOutput(os_type, targetFolder)
	targetFolder = "mapexport_" .. targetFolder
	
	print("�100255100Starting to copy files:")
	mapexport.rmdir(targetFolder)	-- remove the directory before exporting
	
	
	for k,v in pairs(folderList) do
		mapexport.mkdir(targetFolder, k)
		folderCount = folderCount + 1
	end
	
	for k,v in pairs(fileList) do
		mapexport.copy(targetFolder, v)
	end
	
	print("�200200000Finished copying!")
	print(string.format("�200200000Copied a total of %d Folder(s) and %d File(s)! Expected: %d-%d",
		folderCount, #fileList, resTotal, resTotal+4
	))
end

-- LISTEXPORT

function mapexport.doListexport(os_type, targetFolder)

	local folderList, fileList, resTotal = mapexport.captureConsoleOutput(os_type, targetFolder)
	local listexportFile, ioError = io.open("listexport_" .. targetFolder .. ".txt", "w+")
	
	if listexportFile == nil then
		print("�255100100Error writing to file: " .. ioError)
		
		return 
	end
	
	print("�100255100Starting to write to file 'listexport_" .. targetFolder .. ".txt'")
	for k,v in pairs(fileList) do
		listexportFile:write( v:gsub("\\", "/") .. "\n")
	end
	
	listexportFile:close()	
	
	print("�200200000Finished writing!")
	print(string.format("�200200000The list contains %d File(s)! Expected: %d-%d",
		 #fileList, resTotal, resTotal+4
	))
end
