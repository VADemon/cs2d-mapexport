# Mapexport & Listexport script for CS2D

[Official download](https://unrealsoftware.de/files_show.php?file=16267) on Unreal Software

Usage: See Readme.bb.txt

But simply:

1. Add script to `sys/lua/autorun` or load it somehow
2. Type `mapexport` or `listexport` in console. Wait for console windows to disappear.
   * If mapexport doesn't recognise the OS for some reason: `mapexport [windows/linux]`