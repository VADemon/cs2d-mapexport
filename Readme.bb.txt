[TOOL] Mapexport 2.3 + Listexport (Servertransfer)
Welcome, this is a file:16266 rip-off for maps :ugly: 

The point of [i]Mapexport[/i] is simple: Uploading a map that uses a lot of images and sounds is a real pain in the ass... and because I'm a nice guy, I want to save your ass from being hurt!   :par-t: 

 :*: [b]What does Mapexport do?[/b]
Mapexport automatically exports a map (and the files used) to a folder, making it super easy to export the map altogether with its resources.

Assume, you have a big RPG map with 100 used images and sounds. How long will it take you to copy all these files manually, recreating the original folder structure? Probably an hour. Mapexport does it [u]in seconds[/u].

 :*: [b]How to use Mapexport?[/b]
 :o: Start a [i]New Game[/i] with the selected map
 :o: enter "[b]mapexport[/b] [OS type]" in console
 :o: wait a few seconds, you'll see Console windows open - don't worry
  :>:   go to your CS2D folder and you will see a folder called [b]"mapexport_[i]MAPNAME[/i]"[/b]

Yes, it's simple.


 :*:  [b]Command "mapexport"[/b]
[b]Syntax:[/b] mapexport [OS Type]
[b]OS Type[/b] is an optional paramater, it'll try its best to auto-detect your OS.
[b]Values:[/b] windows / linux / mac

 :>: [u]Mapexport[/u] will copy all used map resources to a folder in the root of your CS2D directory ("mapexport_<mapname>"). As this folder contains all files used by the map, it's ready to be shared with friends or be uploaded to Unreal Software.


 :*:  [b]Command "listexport"[/b]
[b]Syntax:[/b] listexport [OS Type]

 :>: [u]Listexport[/u] will make a list of all files used by map and write it to a text file in the root of your CS2D directory. This file is called "listexport_<mapname>.txt" and contains one file path per line.


 :*: [b]Installation[/b]
Put the .lua into the /sys/lua/autorun folder

 :*: [b]Question / Bug reports?[/b]
It's best you send me a private message here for me to notice. Or create a new issue: https://gitlab.com/VADemon/cs2d-mapexport

[more= :*: Changelog  ]
v2.3
[FIXED] Extraction of really weird mixed paths (\ AND / within a map). Thanks user:956
[ADDED] Print expected number of files

v2.2
[FIXED] Linux/Unix: Path escaping.
[INFO] Refactored the script

v2.1
[ADDED] Export Map Tilesets
[FIXED] Use OS Specific directory separators

v2.0
[ADDED] Command "listexport [OS]" which exports all used resources as a list to .txt file
[CHANGED] Mapexport will not run when the type of the OS wasn't specified/auto-detected

v1.3a
[FIXED] Parent folders weren't created automatically on Linux/*unix and it's required because the folder order in the script isn't perfect.
Some testing and thus little improvements in the code.

v1.3
[ADDED] Map's .txt file is now also exported (should have made it's way into version 1.2 :/)

v1.2:
[ADDED] Map's .lua file is now also exported (thanks to user:21431 for reminding)
[ADDED] Output how many folders and files were copied

v1.1:
[FIXED] The previous mapexport folder wasn't deleted when exporting a map more than once.
[CHANGED] Slightly changed printed messages
[/more]